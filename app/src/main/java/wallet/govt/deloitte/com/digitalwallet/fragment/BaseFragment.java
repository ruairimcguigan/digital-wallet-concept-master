package wallet.govt.deloitte.com.digitalwallet.fragment;

import android.graphics.PorterDuff;
import android.support.v4.app.Fragment;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageButton;

/**
 * Created by rmcguigan on 03/09/15.
 */
public class BaseFragment extends Fragment implements View.OnClickListener, View.OnTouchListener{

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        switch ( event.getAction() ) {
            case MotionEvent.ACTION_DOWN: {
                ImageButton view = ( ImageButton ) v;
                //overlay is black with transparency of 0x77 (119)
                view.getDrawable().setColorFilter(0x77000000, PorterDuff.Mode.SRC_ATOP);
                view.invalidate();
                break;
            }
            case MotionEvent.ACTION_UP:
            case MotionEvent.ACTION_CANCEL: {
                ImageButton view = ( ImageButton ) v;
                //clear the overlay
                view.getDrawable().clearColorFilter();
                view.invalidate();
                break;
            }
        }

        return false;
    }

    @Override
    public void onClick(View v) {

    }
}
