package wallet.govt.deloitte.com.digitalwallet.utils;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by rmcguigan on 02/09/15.
 */
public class UserDetailsSharedPreference {

    String getSurname, getLicence, getNatInsurance, getPostCode;

    public static final String PREFS_NAME = "USER";
    public static final String PREFS_KEY_SURNAME = "SURNAME";
    public static final String PREFS_KEY_LICENCE = "LICENCE NUMBER";
    public static final String PREFS_KEY_NAT_INSURANCE = "NATIONAL INSURANCE NUMBER";
    public static final String PREFS_KEY_POST_CODE = "POST CODE";

    public UserDetailsSharedPreference() {
        super();
    }


    public void savePrefValues(Context context, String surname, String licence, String natInsurance, String postCode) {

        SharedPreferences settings;
        SharedPreferences.Editor editor;

        //settings = PreferenceManager.getDefaultSharedPreferences(context);
        settings = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        editor = settings.edit();

        editor.putString(PREFS_KEY_SURNAME, surname);
        editor.putString(PREFS_KEY_LICENCE, licence);
        editor.putString(PREFS_KEY_NAT_INSURANCE, natInsurance);
        editor.putString(PREFS_KEY_POST_CODE, postCode);

        editor.commit(); //4
    }

    public void getPrefValues(Context context) {
        SharedPreferences settings;

        //settings = PreferenceManager.getDefaultSharedPreferences(context);
        settings = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        getSurname = settings.getString(PREFS_KEY_SURNAME, null);
        getLicence = settings.getString(PREFS_KEY_LICENCE, null);
        getNatInsurance = settings.getString(PREFS_KEY_NAT_INSURANCE, null);
        getPostCode = settings.getString(PREFS_KEY_POST_CODE, null);


    }

    public void clearPrefValues(Context context) {
        SharedPreferences settings;
        SharedPreferences.Editor editor;

        //settings = PreferenceManager.getDefaultSharedPreferences(context);
        settings = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        editor = settings.edit();

        editor.clear();
        editor.commit();
    }

    public void removeValue(Context context) {
        SharedPreferences settings;
        SharedPreferences.Editor editor;

        settings = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        editor = settings.edit();

        //editor.remove(PREFS_KEY);
        editor.commit();
    }
}

