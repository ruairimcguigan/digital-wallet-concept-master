package wallet.govt.deloitte.com.digitalwallet.fragment;


import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import wallet.govt.deloitte.com.digitalwallet.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class HMRevenueFragment extends BaseFragment {


    public HMRevenueFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_hmrevenue, container, false);
    }


}
