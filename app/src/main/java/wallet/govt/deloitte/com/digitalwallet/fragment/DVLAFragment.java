package wallet.govt.deloitte.com.digitalwallet.fragment;


import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.Toast;

import wallet.govt.deloitte.com.digitalwallet.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class DVLAFragment extends BaseFragment  {

    ViewGroup mDVLARootView;
    ImageButton mDrivingLicenceOption, mVehicleTaxOption, mVehicleMotOption, mVehicleRegistrationOption;


    public static final DVLAFragment newInstance() {

        DVLAFragment f = new DVLAFragment();
        Bundle bdl = new Bundle(1);
        f.setArguments(bdl);
        return f;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_dvla, container, false);
        init(v);
        //setTouchAndClickEvents();
        mDrivingLicenceOption.setOnClickListener(this);
        mDrivingLicenceOption.setOnTouchListener(this);
        mVehicleTaxOption.setOnClickListener(this);
        mVehicleTaxOption.setOnTouchListener(this);
        mVehicleMotOption.setOnClickListener(this);
        mVehicleMotOption.setOnTouchListener(this);
        mVehicleRegistrationOption.setOnClickListener(this);
        mVehicleRegistrationOption.setOnTouchListener(this);

        return v;
    }

    private void init(View v) {
        mDVLARootView = (ViewGroup)v.findViewById(R.id.dvla_root_view);
        mDrivingLicenceOption = ( ImageButton ) v.findViewById(R.id.fragment_tile_driving_licence);
        mVehicleTaxOption = ( ImageButton ) v.findViewById(R.id.fragment_tile_vehicle_tax);
        mVehicleMotOption = ( ImageButton ) v.findViewById(R.id.fragment_tile_vehicle_mot);
        mVehicleRegistrationOption = ( ImageButton ) v.findViewById(R.id.fragment_tile_vehicle_registration);
    }

//     TODO: Revise this method to search all child views of dvla_root_view - getChildAt() only looks at direct child
//    private void setTouchAndClickEvents(){
//        for(int i=0; i< mDVLARootView.getChildCount(); i++){
//            View view = mDVLARootView.getChildAt(i);
//            if(view instanceof ImageButton){
//                ImageButton button = (ImageButton)view;
//                button.setOnClickListener(this);
//                button.setOnTouchListener(this);
//            }
//        }
//    }

    @Override
    public void onClick(View v) {
        switch ( v.getId() ) {
            case R.id.fragment_tile_driving_licence:
                Toast.makeText(getActivity(), "Driving licence", Toast.LENGTH_SHORT).show();
                break;
            case R.id.fragment_tile_vehicle_tax:
                Toast.makeText(getActivity(), "Tax", Toast.LENGTH_SHORT).show();
                break;
            case R.id.fragment_tile_vehicle_mot:
                Toast.makeText(getActivity(), "MOT", Toast.LENGTH_SHORT).show();
                break;
            case R.id.fragment_tile_vehicle_registration:
                Toast.makeText(getActivity(), "Vehicle Reg", Toast.LENGTH_SHORT).show();
        }
    }

}






