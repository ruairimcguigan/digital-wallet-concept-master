package wallet.govt.deloitte.com.digitalwallet;

import android.app.Application;

import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

/**
 * Created by rmcguigan on 01/09/15.
 */
public class BaseApplication extends Application {

    private static final String TAG = BaseApplication.class.getSimpleName();

    @Override
    public void onCreate() {
        super.onCreate();

        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                        .setDefaultFontPath("fonts/OpenSans-Light.otf")
                        .setFontAttrId(R.attr.fontPath)
                        .build()
        );
    }


}
