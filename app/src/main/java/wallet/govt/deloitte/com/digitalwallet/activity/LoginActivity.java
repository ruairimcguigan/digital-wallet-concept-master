package wallet.govt.deloitte.com.digitalwallet.activity;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import butterknife.Bind;
import butterknife.ButterKnife;
import wallet.govt.deloitte.com.digitalwallet.R;
import wallet.govt.deloitte.com.digitalwallet.activity.base.BaseActivity;

public class LoginActivity extends BaseActivity implements TextWatcher{

    String mPin1, mPin2, mPin3, mPin4;
    String mUserLoginPin;
    private static final int PIN_VIEW_MAX_LENGTH = 1;

    @Bind(R.id.activity_login_pin_1) EditText mLoginPinOneView;
    @Bind(R.id.activity_login_pin_2) EditText mLoginPinTwoView;
    @Bind(R.id.activity_login_pin_3) EditText mLoginPinThreeView;
    @Bind(R.id.activity_login_pin_4) EditText mLoginPinFourView;
    @Bind(R.id.activity_login_submit_button)Button mLoginSubmitButton;
    @Bind(R.id.login_pin_root_view) ViewGroup mLoginPinRootView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        pinViewTextChangeListener();

        mLoginSubmitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LogUserIn();
            }
        });
    }

    private void pinViewTextChangeListener(){
        for(int i = 0; i < mLoginPinRootView.getChildCount(); i++){
            View v = mLoginPinRootView.getChildAt(i);
            if(v instanceof EditText){
                EditText pinView = (EditText) v;
                pinView.addTextChangedListener(this);
            }
        }
    }

    private void LogUserIn() {
        getUserPinInput();
        collateUserPin();
        validateUserLoginPin();
    }

    private void getUserPinInput() {
        mPin1 = mLoginPinOneView.getText().toString().trim();
        mPin2 = mLoginPinTwoView.getText().toString().trim();
        mPin3 = mLoginPinThreeView.getText().toString().trim();
        mPin4 = mLoginPinFourView.getText().toString().trim();
    }

    protected String collateUserPin(){
        mUserLoginPin = new StringBuilder().append(mPin1).append(mPin2)
                .append(mPin3).append(mPin4).toString();
        return mUserLoginPin;
    }

    private void validateUserLoginPin() {

        SharedPreferences pref = getSharedPreferences("USER", MODE_PRIVATE);
        String pin = pref.getString("USER_PIN", "");

        if ( collateUserPin().equals(pin) ) {
            Toast.makeText(getApplicationContext(), R.string.successfully_logged_in, Toast.LENGTH_SHORT).show();
            moveToActivity(HomeActivity.class);
        }
        else {
            Toast.makeText(getApplicationContext(), R.string.incorrect_pin_entered,
                    Toast.LENGTH_SHORT).show();
            clearPinViews();
            mLoginPinOneView.requestFocus();
        }
    }
        private void clearPinViews(){
        for (int i = 0; i < mLoginPinRootView.getChildCount(); i++) {
            View view = mLoginPinRootView.getChildAt(i);
            if (view instanceof EditText) {
                EditText editText = (EditText)view;
                editText.setText("");
            }
        }
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
       if(mLoginPinOneView.getText().toString().length() == PIN_VIEW_MAX_LENGTH){
           mLoginPinTwoView.requestFocus();
       }
        if(mLoginPinTwoView.getText().toString().length() == PIN_VIEW_MAX_LENGTH){
            mLoginPinThreeView.requestFocus();
        }
        if(mLoginPinThreeView.getText().toString().length() == PIN_VIEW_MAX_LENGTH){
            mLoginPinFourView.requestFocus();
        }
        if(mLoginPinFourView.getText().toString().length() == PIN_VIEW_MAX_LENGTH){
            hideSoftKeyboard();
        }
    }

    @Override
    public void afterTextChanged(Editable s) {}
}
