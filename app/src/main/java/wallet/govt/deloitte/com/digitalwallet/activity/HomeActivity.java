package wallet.govt.deloitte.com.digitalwallet.activity;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.Menu;
import android.view.MenuItem;

import wallet.govt.deloitte.com.digitalwallet.R;
import wallet.govt.deloitte.com.digitalwallet.activity.base.BaseActivity;
import wallet.govt.deloitte.com.digitalwallet.fragment.DVLAFragment;

public class HomeActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        ViewPager pager = ( ViewPager ) findViewById(R.id.homeViewPager);
        pager.setAdapter(new MenuPageAdapter(getSupportFragmentManager()));
    }


    public class MenuPageAdapter extends FragmentPagerAdapter {
        public MenuPageAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            switch (position){
                case 1:
                    return DVLAFragment.newInstance();
                case 2:
                    return DVLAFragment.newInstance();
                case 3:
                    return DVLAFragment.newInstance();
                default:
                    return DVLAFragment.newInstance();

            }
        }

        @Override
        public int getCount() {
            return 3;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if ( id == R.id.action_settings ) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
