package wallet.govt.deloitte.com.digitalwallet.activity;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;

import butterknife.Bind;
import butterknife.ButterKnife;
import wallet.govt.deloitte.com.digitalwallet.R;
import wallet.govt.deloitte.com.digitalwallet.activity.base.BaseActivity;
import wallet.govt.deloitte.com.digitalwallet.utils.UserDetailsSharedPreference;

public class RegisterActivity extends BaseActivity {

    UserDetailsSharedPreference sharedPreferences;
    String mSurnameString, mDrivingNumberString, mNationalInsuranceString, mPostCodeString;
    Activity context = this;

    @Bind(R.id.activity_register_surname_input) EditText mSurnameInput;
    @Bind(R.id.activity_register_driving_number_input) EditText mDrivingNumberInput;
    @Bind(R.id.activity_register_national_insurance_input) EditText mNationalInsuranceInput;
    @Bind(R.id.activity_register_post_code_input) EditText mPostCodeInput;
    @Bind(R.id.activity_register_radio_agree) RadioButton mRadioButtonInput;
    @Bind(R.id.activity_register_submit_button) Button mRegisterSubmitButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.activity_register);
        ButterKnife.bind(this);

        sharedPreferences = new UserDetailsSharedPreference();
        mRegisterSubmitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getUserDetails();
            }
        });
    }

    protected void getUserDetails() {
        mSurnameString = mSurnameInput.getText().toString().trim();
        mDrivingNumberString = mDrivingNumberInput.getText().toString().trim();
        mNationalInsuranceString = mNationalInsuranceInput.getText().toString().trim();
        mPostCodeString = mPostCodeInput.getText().toString().trim();

        sharedPreferences.savePrefValues(context, mSurnameString, mDrivingNumberString, mNationalInsuranceString, mPostCodeString);
        moveToActivity(SetPinActivity.class);

    }

}

