package wallet.govt.deloitte.com.digitalwallet.activity;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import butterknife.Bind;
import butterknife.ButterKnife;
import wallet.govt.deloitte.com.digitalwallet.R;
import wallet.govt.deloitte.com.digitalwallet.activity.base.BaseActivity;

public class SetPinActivity extends BaseActivity  implements TextWatcher {

    String mPin1, mPin2, mPin3, mPin4, mUserPin;
    private static final int MAX_PIN_VIEW_LENGTH = 1;

    @Bind(R.id.activity_set_pin_1) EditText mSetPinOneView;
    @Bind(R.id.activity_set_pin_2) EditText mSetPinTwoView;
    @Bind(R.id.activity_set_pin_3) EditText mSetPinThreeView;
    @Bind(R.id.activity_set_pin_4) EditText mSetPinFourView;
    @Bind(R.id.activity_set_pin_submit_button)Button mSetPinSubmitButton;
    @Bind(R.id.activity_set_pin_root_view) ViewGroup mSetPinRootView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_set_pin);
        ButterKnife.bind(this);
        pinViewTextChangeListener();
        saveUserDetails();
    }

    private void pinViewTextChangeListener() {
        for (int i = 0; i < mSetPinRootView.getChildCount(); i++){
            View view = mSetPinRootView.getChildAt(i);
            if(view instanceof EditText){
                EditText pinView = (EditText) view;
                pinView.addTextChangedListener(this);
            }
        }
    }

    private void getUserPinInput() {
        mPin1 = mSetPinOneView.getText().toString().trim();
        mPin2 = mSetPinTwoView.getText().toString().trim();
        mPin3 = mSetPinThreeView.getText().toString().trim();
        mPin4 = mSetPinFourView.getText().toString().trim();
    }

    protected String collateUserPin(){
        mUserPin = new StringBuilder().append(mPin1).append(mPin2).append(mPin3).append(mPin4).toString();
        return mUserPin;
    }

    private void saveUserDetails() {
        mSetPinSubmitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getUserPinInput();
                collateUserPin();
                accessUserSharedPreferences();
                Toast.makeText(getApplicationContext(), "You have successfully registered, now please log in with your unique PIN",
                        Toast.LENGTH_SHORT).show();
                moveToActivity(LoginActivity.class);
            }
        });
    }

    private void accessUserSharedPreferences(){
        SharedPreferences pref = getSharedPreferences("USER", MODE_APPEND);
        SharedPreferences.Editor editor = pref.edit();
        editor.putString("USER_PIN", collateUserPin());
        editor.commit();
    }


    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

        if(mSetPinOneView.getText().toString().length() == MAX_PIN_VIEW_LENGTH){
            mSetPinTwoView.requestFocus();
        }
        if( mSetPinTwoView.getText().toString().length() == MAX_PIN_VIEW_LENGTH){
            mSetPinThreeView.requestFocus();
        }
        if(mSetPinThreeView.getText().toString().length() == MAX_PIN_VIEW_LENGTH){
            mSetPinFourView.requestFocus();
        }
        if(mSetPinFourView.getText().toString().length() == MAX_PIN_VIEW_LENGTH){
            hideSoftKeyboard();
        }
    }

    @Override
    public void afterTextChanged(Editable s) {

    }
}
